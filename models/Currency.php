<?php
/**
 * Created by PhpStorm.
 * User: max
 * Name: Cherednyk Maxim
 * Phone: +380639960375
 * Email: maks757q@gmail.com
 * Date: 01.02.2018
 * Time: 00:50
 */

namespace app\models;


use yii\base\Model;
use yii\base\Module;
use yii\httpclient\Client;

/**
* @property $date array
* @property $data array
*/

class Currency extends Model
{
    const UAH = 'UAH';
    const RUB = 'RUB';
    const EUR = 'EUR';

    private static $key = 'currency_cache';
    public $date = [];
    public $data = [];

    public function getCurrencyMonthHistory($currency = Currency::UAH)
    {
        $cache = \Yii::$app->cache->get(static::$key.$currency);
        if(empty($cache)) {
            $data = [];
            $time = time();

            for ($i = 0; $i < 7; $i++) {
                $date = date('Y-m-d', strtotime("-$i day", $time));

                $client = new Client();
                $response = $client->createRequest()
                    ->setMethod('GET')
                    ->setUrl("http://apilayer.net/api/historical")
                    ->setData([
                        'access_key' => '0c88ecdbdbc3fff258521d976b0f9aac',
                        'date' => $date,
                        'currencies' => $currency,
                        'format' => 1
                    ])
                    ->send();
                if ($response->isOk && isset($response->data['quotes']['USD'.$currency])) {
                    $curs = $response->data['quotes']['USD'.$currency];
                    $data['date'][] = $date;
                    $data['data'][] = $curs;
                }
            }
            \Yii::$app->cache->set(static::$key.$currency, $data, 6*60*60);
            $this->date = $data['date'];
            $this->data = $data['data'];
            return true;
        } else {
            $this->date = $cache['date'];
            $this->data = $cache['data'];
            return true;
        }
    }
}