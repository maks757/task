<?php

namespace app\controllers;


use app\models\Currency;
use yii\web\Controller;

class CurrencyController extends Controller
{
    public function actionIndex($currency = Currency::UAH)
    {
        $currencies = new Currency();
        $currencies->getCurrencyMonthHistory($currency);
        return $this->render('index', ['currencies' => $currencies, 'currency' => $currency]);
    }
}