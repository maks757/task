<?php

use dosamigos\chartjs\ChartJs;

/* @var $currencies \app\models\Currency */
/* @var $currency string */

?>

<?php \yii\widgets\Pjax::begin(['options' => ['class' => 'pjax-main']]) ?>

<a href="<?= \yii\helpers\Url::toRoute(['/currency', 'currency' => \app\models\Currency::RUB]) ?>">RUB</a>
<a href="<?= \yii\helpers\Url::toRoute(['/currency', 'currency' => \app\models\Currency::UAH]) ?>">UAH</a>
<a href="<?= \yii\helpers\Url::toRoute(['/currency', 'currency' => \app\models\Currency::EUR]) ?>">EUR</a>

<div class="text-center">
    <div class="loader"></div>
</div>


<?= ChartJs::widget([
    'type' => 'line',
    'options' => [
        'height' => 200,
        'width' => 400
    ],
    'data' => [
        'labels' => $currencies->date,
        'datasets' => [
            [
                'label' => 'USD to ' . $currency,
                'backgroundColor' => "rgba(255,99,132,0.2)",
                'borderColor' => "rgba(255,99,132,1)",
                'pointBackgroundColor' => "rgba(255,99,132,1)",
                'pointBorderColor' => "#fff",
                'pointHoverBackgroundColor' => "#fff",
                'pointHoverBorderColor' => "rgba(255,99,132,1)",
                'data' => $currencies->data
            ],
        ]
    ]
]);
?>

<?php \yii\widgets\Pjax::end() ?>
